# Pluralsight: Unit Testing In Python

Work related to following along with the [Unit Testing In Python](https://app.pluralsight.com/library/courses/using-unit-testing-python/table-of-contents) course, a member of the [Python Skills Learning Path](https://app.pluralsight.com/paths/skills/python).

All tests should follow the logical progression of:

- Arrange
- Act
- Assert

    ```python
    # example unit test
    def test_lookup_by_name(self):
        self.phonebook.add("Bob", "12345")     # arrange
        number = self.phonebook.lookup("Bob")  # act
        self.assertEqual("12345", number)      # assert
    ```

## Running unittest

```bash
# run this in the same directory as your test file(s)
python -m unittest
```
