#!/usr/local/bin/python3

import unittest
from phonebook import PhoneBook


class PhoneBookTest(unittest.TestCase):

    # called before every test method
    def setUp(self) -> None:
        self.phonebook = PhoneBook()

    # # called after every test method
    # def tearDown(self) -> None:

    
    def test_lookup_by_name(self):
        self.phonebook.add("Bob", "12345")
        number = self.phonebook.lookup("Bob")
        self.assertEqual("12345", number)

    def test_missing_name(self):
        with self.assertRaises(KeyError):
            self.phonebook.lookup("missing")

    def test_is_consistent_with_multiple_names(self):
        self.phonebook.add("Bob", "12345")
        self.phonebook.add("Sue", "012345")
        self.assertTrue(self.phonebook.is_consistent())

    def test_is_inconsistent_with_duplicate_entries(self):
        self.phonebook.add("Bob", "12345")
        self.phonebook.add("Sue", "12345")
        self.assertFalse(self.phonebook.is_consistent())

    def test_inconsistent_with_duplicate_prefix(self):
        self.phonebook.add("Bob", "12345")
        self.phonebook.add("Sue", "123")  # prefix of Bob
        self.assertFalse(self.phonebook.is_consistent())

    def test_phonebook_adds_name_and_number(self):
        self.phonebook.add("Sue", "12345")
        self.assertIn("Sue", self.phonebook.get_names())
        self.assertIn("12345", self.phonebook.get_numbers())
